import Vue from 'vue'

export const state = () => ({
    changeText: null,
    prevState: [null,null,null,null],
    postState: [null,null,null,null],
    selectedText: null
})

export const mutations = {
    SET_TEXT: (state, text) => {
        localStorage.setItem('text', text);

        for (var i = 3; i > -1; i--) {
            Vue.set(state.prevState, i, i === 0 ? text : state.prevState[i - 1])
        }

        for (var i = 3; i > -1; i--) {
            state.postState[i] = null
        }
    },
    SET_PREVIOUS_TEXT: (state) => {

        if(state.prevState[1]) {
            state.changeText = state.prevState[1];

            for (var i = 3; i > -1; i--) {
                Vue.set(state.postState, i, i === 0 ? state.prevState[0] : state.postState[i - 1])
            }

            for (var i = 0; i < 4; i++) {
                Vue.set(state.prevState, i, i === 3 ? null : state.prevState[i + 1])
            }
        }
    },
    SET_POST_TEXT: (state) => {

        if(state.postState[0]) {
            state.changeText = state.postState[0];

            for (var i = 3; i > -1; i--) {
                Vue.set(state.prevState, i, i === 0 ? state.postState[0] : state.prevState[i - 1])
            }

            for (var i = 0; i < 4; i++) {
                Vue.set(state.postState, i, i === 3 ? null : state.postState[i + 1])
            }
        }
    },
    SET_SELECTION: (state, text) => state.selectedText = text,
    SET_HEADER: (state, currText) => {
        if(currText.search(state.selectedText) !== -1) {
            let firstInd = currText.search(state.selectedText);
            let secondInd = currText.search(state.selectedText) + state.selectedText.length;

            state.changeText = currText.slice(0,firstInd) + "<span style='font-size:31px'>" + currText.slice(firstInd,secondInd) + "</span>" + currText.slice(secondInd,currText.length);

            for (var i = 3; i > -1; i--) {
                Vue.set(state.prevState, i, i === 0 ? state.changeText : state.prevState[i - 1])
            }

            for (var i = 3; i > -1; i--) {
                state.postState[i] = null
            }
        }
    },
    SET_PARAGRAPH: (state, currText) => {
        if(currText.search(state.selectedText) !== -1) {
            let firstInd = currText.search(state.selectedText);
            let secondInd = currText.search(state.selectedText) + state.selectedText.length;

            state.changeText = currText.slice(0,firstInd) + "<br><div><p>" + currText.slice(firstInd,secondInd) + "</p></div><br>" + currText.slice(secondInd,currText.length);

            for (var i = 3; i > -1; i--) {
                Vue.set(state.prevState, i, i === 0 ? state.changeText : state.prevState[i - 1])
            }

            for (var i = 3; i > -1; i--) {
                state.postState[i] = null
            }
        }

    },
    SET_IMAGE_URL: (state, body) => {
        state.changeText = body.currText + "<div><br><img style='width: 600px;' src='" + body.imgUrl + "'><br></div>";

        for (var i = 3; i > -1; i--) {
            Vue.set(state.prevState, i, i === 0 ? state.changeText : state.prevState[i - 1])
        }

        for (var i = 3; i > -1; i--) {
            state.postState[i] = null
        }

    }
}

export const actions = {
    setText({ state, commit }, text) {
        commit('SET_TEXT', text)
    },
    setPreviousText({ state, commit }) {
        commit('SET_PREVIOUS_TEXT')
    },
    setPostText({ state, commit }) {
        commit('SET_POST_TEXT')
    },
    setSelection({ state, commit }, text) {
        commit('SET_SELECTION', text)
    },
    setHeader({ state, commit }, currText) {
        commit('SET_HEADER', currText)
    },
    setParagraph({ state, commit }, currText) {
        commit('SET_PARAGRAPH', currText)
    },
    uploadImage({ state, commit }, body) {
        commit('SET_IMAGE_URL', body)
    }
}
